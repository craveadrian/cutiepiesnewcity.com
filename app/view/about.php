<div id="content">
	<div class="row">
		<h1>About My Program</h1>
   		
   		<p>Cutie Pies New City Home Daycare is a state licensed home daycare (also known as family daycare or in-home daycare) located in New City, NY (Rockland County).  Although I have a larger license, I operate with only 4 children on average, to give more one-on-one attention to the children in my care. My goal is to provide a fun and safe learning environment for your child, while encouraging social, emotional, intellectual, and physical development. Your child will receive quality personal and individualized care in a warm and loving home, where they can feel safe and happy and can begin to build confidence and a positive self-image.</p>

   		<p>My home is designed to provide both a nurturing home atmosphere along with large dedicated areas for the children to actively learn and play in, a stimulating environment with spacious rooms rich in color giving each child an exciting world full of opportunities to explore, learn, and grow and reach their full potential.</p>

   		<p>I have a large fully fenced, child friendly backyard, equipped with fun and safe playing equipment. </p>

   		<p>I also have a vegetable garden that children will enjoy, explore and learn about. </p>

   		<p>I strongly believe that nutrition is a vital part of a child's health. It is very important for me as a mother and a childcare provider. Children need to eat well-balanced meals in order to meet their daily energy needs and to help them build a strong body and mind. Therefore, I do my best to provide your child with mostly organic, healthy, well balanced nutritious meals and snacks. I go well and beyond the requirements of the CACFP (Child and Adult Care Food Program) that I participate in.</p>

   		<p>I believe that children should be allowed to learn and grow at their own pace, and that a child-centered, play-based curriculum is the most appropriate and effective method of learning for young children.</p>

   		<p>Despite the recent "push-down" of academics to younger and younger children, studies show that play is the best way for children to learn.</p>

   		<p>For children of all ages I incorporate lots of fun activities into their daily schedule that include arts and crafts, free-play, structured-play, music, song and dance, painting, coloring, various creative and educational projects, and much more. Additionally, for children ages two and up I we offer an age appropriate preschool curriculum. Therefore, I feel I can provide the best of both worlds (learning and fun, all in one).</p>

   		<p>My goals for children's learning is for them to become independent, self-confident, and enthusiastic learners for life, developing strong self-esteem and positive attitudes toward exploration and learning so they're not afraid to try out new ideas or make mistakes.</p>

   		<p>Our family daycare meets all the requirements established by the New York State Office of Children and Family Services. I believe licensing promotes the highest quality child care by requiring minimum standards for the care and protection of children. At least twice yearly a state licensing specialist pays a surprise visit to evaluate my program with regards to health and safety, adult-child ratio, caregiver training, equipment, nutrition, behavior and guidance procedures, and other hallmarks of quality child care. The results of these visits are always available to the public.</p>

   		<p>I understand that you are trusting me to care for your child, and I take this responsibility very seriously. I look forward to the opportunity to care for your child, while providing a positive influence in their life.</p>

   		<h3>My Philosophy</h3>

   		<h2>Inspiring & Encouraging Children to Play, Learn, Experiment and Get Messy</h2>

   		<p>Cutie Pies New City Home Daycare provides licensed care for infants through school-age children in a loving, developmentally appropriate home environment that will allow each child to grow to his or her potential. My goal is to assist your child in their social, emotional, intellectual and physical development, to build each child's self-confidence and to do so in an environment that provides both fun and excitement while stimulating their imagination. Each age and stage brings its own set of changes and developments and I strive to help each child make discoveries about themselves and their world on a day-to-day basis. For parents, I hope to be your partner and your friend as we have the same goal, to provide love and support for your child.</p>

	</div>
</div>
