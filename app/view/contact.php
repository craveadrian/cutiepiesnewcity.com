<div id="content">
	<div class="row">
		<h1>Contact Us</h1>
	
		<h2>Looking for Family Child Care?</h2>
		<p>If you're looking for family child care in the Clarkstown/New City area, you owe it to yourself and your child to take a closer look at my program and allow me the privilege of caring for your little one during his/her early years. My child care program is conveniently located in New City; within less than a mile distance from Felix Festa Middle School & Link Elementary, Palisades (Exit 10) and Route 304 (right off Germonds Road).</p>

		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6011.097586191426!2d-73.99955614730813!3d41.12254430473665!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2c368e80ff66f%3A0xb2a095ee99d57109!2sCutie+Pies+New+City+Daycare!5e0!3m2!1sen!2sph!4v1532111090863" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

		<p>Contact me for an interview and tour of my program - you'll be glad you did!</p>
		<p>We would love to answer any questions that you may have about Cutie Pies New City Daycare.  Please contact us for more information:</p>

		<p><b>Phone:</b> <?php $this->info(["phone","tel"]) ?></p>
		<p><b>Fax:</b> <?php $this->info(["phone","tel"]) ?></p>
		<p><b>Website:</b> <a href="cutiepiesnewcity.com">cutiepiesnewcity.com</a></p>
		<p><b>Address:</b> <?php $this->info("address") ?></p>
		<p><b>Email:</b> <?php $this->info(["email","mailto"]) ?></p>


		<h2>Contact Form</h2>

		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<label><span class="ctc-hide">Name</span>
				<input type="text" name="name" placeholder="Name:">
			</label>
			<label><span class="ctc-hide">Address</span>
				<input type="text" name="address" placeholder="Address:">
			</label>
			<label><span class="ctc-hide">Email</span>
				<input type="text" name="email" placeholder="Email:">
			</label>
			<label><span class="ctc-hide">Phone</span>
				<input type="text" name="phone" placeholder="Phone:">
			</label>
			<label><span class="ctc-hide">Message</span>
				<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
			</label>
			<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
			<div class="g-recaptcha"></div>
			<label>
				<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
			</label><br>
			<?php if( $this->siteInfo['policy_link'] ): ?>
			<label>
				<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
			</label>
			<?php endif ?>
			<button type="submit" class="ctcBtn" disabled>Submit</button>
		</form><br />

        
        <p><b>Child Care Hours</b></p>
<p>Monday-Friday: Depending on each client's needs, typically between 7:30 am-5:30 pm</p>

	</div>
</div>


