<div id="content">
	<div id="welcome-section">
		<div class="row">
			<div class="welcome-left">
				<img src="public/images/content/img1.jpg" alt="playground">
			</div>
			<div class="welcome-right">
				<h2>Welcome to the,</h2>
				<h1><span class="color1">Cutie Pies</span> <span class="color2">New City</span> </h1>
				<!-- <p>Cutie Pies New City Home Daycare is a state licensed home daycare (also known as family daycare or in-home daycare) located in New City, NY (Rockland County). Although I have a larger license, I operate with only 4 children on average, to give more one-on-one attention to the children in my care. My goal is to provide a fun and safe learning environment for your child, while encouraging social, emotional, intellectual, and physical development. Your child will receive quality personal and individualized care in a warm and loving home, where they can feel safe and happy and can begin to build confidence and a positive self-image.</p>

<p>My home is designed to provide both a nurturing home atmosphere along with large dedicated areas for the children to actively learn and play in, a stimulating environment with spacious rooms rich in color giving each child an exciting world full of opportunities to explore, learn, and grow and reach their full potential.</p> -->
				<ul>
               <li><p> NYS licensed & insured</p></li>
					<li><p>Safe & secure environment</p></li>
					<li><p>CPR, First Aid, Child Care Health & Safety certified</p></li>
					<li><p>Caring, competent, and compassionate staff</p></li>
					<li><p>Certified, developmentally appropriate curriculum</p></li>
					<li><p>Organic, healthy homemade, snacks and meals</p></li>
					<li><p>Daily outside play time in a fully fenced large backyard</p></li>
					<li><p>Increased Parent Engagement/Communication</p></li>
					<li><p>Instant Communication During the day via our XXXXXX app</p></li>
					<li><p>Parents can communicate directly with us as well as receive pictures and videos</p></li>
				</ul>
				<a href="<?php echo URL ?>about#content" class="btn">Learn More</a>
			</div>
		</div>
	</div>
	<div id="our-program-section">
		<div class="row">
			<h2>Program</h2>
			<p>Cutie Pies New City Daycare offers warm and inviting spaces with accessible toys and quality learning materials</p>
			<p>Children have appropriate areas to find quiet time, read, build, imagine, play and discover.</p>
			<dl>
				<dt> <img src="public/images/content/Fun&Exciting Learning.jpg" alt="Placeholder"></dt>
				<dd class="prDayCare"> Fun & Exciting Learning </dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/day_care.jpg" alt="Day Care"> </dt>
				<dd class="prDayCare"> Physical Development </dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/toddlers.jpg" alt="Toddler"> </dt>
				<dd class="prToddler"> Social Emotional Development </dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/creative.jpg" alt="creative"> </dt>
				<dd class="prCreative"> Creative </dd>
			</dl>
			</div>
	</div>
	<div id="meet-section">
		<div class="row">
			<h2>ABOUT US</h2>
			<p>All our teachers are highly educated and have years of experience in education. They do their best to ensure fascinating and educational lessons, so your children get either basic knowledge and social skills they can implement in future.</p>
			<dl>
				<dt> <img src="public/images/content/prog1.jpg" alt="creative"> </dt>
				<dd class="prCreative"> Nutrition </dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/prog1.jpg" alt="creative"> </dt>
				<dd class="prCreative"> Daily Routine </dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/prog1.jpg" alt="creative"> </dt>
				<dd class="prCreative"> Parent Area </dd>
			</dl>
		</div>
	</div>
	<div id="testimonials-section">
		<div class="row">
			<h2>Parents Testimonials</h2>
			<img src="public/images/content/testImage.png" alt="">
			<p>“Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.”</p>
			<p class="testName">EMMA DOE</p>
		</div>
	</div>
</div>
